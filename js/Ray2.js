class Ray2{

	/**
	 * @param {Vec2} origin the starting point of the ray
	 * @param {Vec2} directionNormal a vector that represents the direction the ray points in
	 * @param {Number} length the max distance the ray will be casted
	 */
	constructor(origin, directionNormal, length){

		/**@type {Vec2} */
		this._origin = origin;
		/**@type {Vec2} */
		this._directionNormal = directionNormal.normalized;
		/**@type {Number} */
		this.length = length;
	}
	/**
	 * @type {Ray2} creates a Ray2 from a specified start and end point
	 * @param {Vec2} start 
	 * @param {Vec2} end 
	 */
	static FromPoints(start, end){
		let dif = end.Subtract(start);
		return new Ray2(start, dif.normalized, dif.magnitude);
	}

	/**@type {Ray2} */
	get clone(){ return new Ray2(this._origin.clone, this._directionNormal.clone, this.length); }

	/**@type {Vec2} de-referenced getter/setter for _origin */
	get origin(){ return this._origin.clone; }
	set origin(value){ this._origin = value.clone; }

	/** @type {Vec2} de-referenced getter/setter for _directionNormal */
	get directionNormal(){ return this._directionNormal.clone; }
	set directionNormal(value){ this._directionNormal = value.normalized; }

	/**@type {Vec2} */
	get endPoint(){ return this._origin.Add(this._directionNormal.Multiply(this.length)); }
	/**@type {Rect} */
	get boundingBox(){ return new Rect(this._origin, this.endPoint); }

	/**
	 * @type {Ray2} returns a ray that's scaled by the specified factor
	 * @param {Vec2} delta 
	 */
	Scale(delta){
		
		/**
		 * if the scale has a 1:1 aspect ratio, we can optomize the
		 * function by cutting out one of the TranformPoint calls, and 
		 * some unnecessary Trigonometric calculations that appear inside 
		 * of Ray2.FromPoints
		 */
		if(delta.x == delta.y){
			return new Ray2(this._origin.Multiply(delta), this._directionNormal.clone, this.length * delta.x);
		}

		let sp = this._origin.Scale(delta);
		let ep = this.endPoint.Scale(delta);
		let r = Ray2.FromPoints(sp, ep);

		return r;
	}

	/**
	 * @type {Vec2} returns the intersection point between two rays, or null if they don't intersect
	 * @param {Ray2} ray 
	 */
	Intersection(ray){
		return Ray2.RayIntersect(this, ray);
	}
	
	/**
	 * @type {Boolean} a quick check to see if the Ray2 collides with a Rect object - untested
	 * @param {Rect} rect 
	 */
	IntersectsRect(rect){

		let dirfrac = new Vec2(1 / this._directionNormal.x, 1 / this._directionNormal.y);

		// lb is the corner of Rect with minimal coordinates - left bottom, rt is maximal corner
		// r.org is origin of ray
		let t1 = (rect.min.x - this._origin.x) * dirfrac.x;
		let t2 = (rect.max.x - this._origin.x) * dirfrac.x;
		let t3 = (rect.min.y - this._origin.y) * dirfrac.y;
		let t4 = (rect.max.y - this._origin.y) * dirfrac.y;

		let tmin = Math.max(Math.max(Math.min(t1, t2), Math.min(t3, t4)));
		let tmax = Math.min(Math.min(Math.max(t1, t2), Math.max(t3, t4)));

		// if tmax < 0, the Rect is behind the ray's origin
		if (tmax < 0){
		    return false;
		}

		// if tmin > tmax, ray doesn't intersect Rect
		if (tmin > tmax){
		    return false;
		}

		// tmin is the distance of the collision from the ray's origin
		return tmin <= this.length;
	}

	/**
	 * Renders the ray from the origin to it's endpoint onto the specified canvas context
	 * @param {CanvasRenderingContext2D} ctx 
	 * @param {string} strokestyle 
	 * @param {Number} thickness 
	 * @param {Vec2} offset
	 */
	RenderLine(ctx, strokestyle, thickness, offset = new Vec2()){

		ctx.strokeStyle = strokestyle;
		ctx.lineWidth = thickness;

		let start = this._origin.Add(offset);
		ctx.beginPath();
		ctx.moveTo(start.x, start.y);
		ctx.lineTo(this.endPoint.x, this.endPoint.y);
		ctx.stroke();
	}

	/**
	 * @type {Vec2} calculates the intersection point between two lines given two
	 *  reference points and their normalized direction vectors
	 * @param {Vec2} p any point that lies on line A
	 * @param {Vec2} q any point that lies on line B
	 * @param {Vec2} dirA the direction vector that line A follows
	 * @param {Vec2} dirB the direction vector that line B follows
	 */
	static LineIntersect(p, q, dirA, dirB){
		
		/*  In a nutshell:
		 *  this segment of code takes the current ray (ray1)
		 * and projects the test ray (ray2) onto a coordinate
		 * system derived from the orientation of ray1(local 
		 * coordinate system), using ray1's origin as the
		 * local coordinate system's origin point.
		 *  At that point you can find the intersection point
		 * by testing ray2's x-intercept (where it's local y
		 * value is equal to zero)
		 */
		let dif = q.Subtract(p);
		/*
		 * 'r' is the projected coorinate system's x-axis, as
		 *   a normalized vector
		 * 'r.x' is the x component of that x-axis
		 * 'r.y' is the y component of that x-axis
		 */
		let r = dirA;
		/*
		 * 'rt' is the projected coordinate system's y-axis, 
		 *   as a normalized vector
		 * 'rxt' is the x component of that y-axis, there is
		 *   no need to store the y componen 'ryt' because it
		 *   us equal to rx
		 */ 
		let rxt = -r.y;
		/*
		 * 'q' is ray2's origin point projected on to the 
		 *   local coordinate system, calculated by applying
		 *   dot product of 'dif' and 'r'
		 * 'qx' and 'qy' are it's corresponding x and y 
		 *   components 
		 */
		let qx = dif.x * r.x + dif.y * r.y;
		let qy = dif.x * rxt + dif.y * r.x; // 'r.x' here is actually representing 'ryt'
		/*
		 * 's' is ray2's direction vector projected on to the
		 *   local coordinate system, calculated by applying
		 *   dot product of ray2's direction vector and 'r'
		 * 'sx' and 'sy' are it's corresponding x and y 
		 *   components 
		 */
		let sx = dirB.x * r.x + dirB.y * r.y;
		let sy = dirB.x * rxt + dirB.y * r.x; // 'r.x' here is actually representing 'ryt'

		/**
		 *  if the y component of ray2's projected direction 
		 * vector, which means the rays are parallel and will 
		 * never intersect, so we return null
		 */
		if (sy == 0) return null;
		
		/**
		 * 'a' is the distance from ray1's origin to the 
		 *   intersection point
		 */
		let a = qx - qy * sx / sy;
		let intersect = new Vec2(p.x + a * r.x, p.y + a * r.y);

		return intersect;
	}

	/**
	 * @type {Vec2} returns the intersect point between two rays if they intersect,
	 *  otherwise returns null
	 * @param {Vec2} rayA 
	 * @param {Vec2} rayB 
	 */
	static RayIntersect(rayA, rayB){

		// no intersect if bounding boxes don't collide
		let aabba = rayA.boundingBox;
		let aabbb = rayB.boundingBox;
		if(!aabba.OverlapsInclusive(aabbb)) return null;

		let intersect = Ray2.LineIntersect(rayA._origin, rayB._origin, rayA._directionNormal, rayB._directionNormal);

		// return null if intersect does not exist, or is not inside both rays' bounding box
		if(intersect == null || !aabba.ContainsPoint(intersect) || !aabbb.ContainsPoint(intersect)) return null;

		return intersect;
	}
}