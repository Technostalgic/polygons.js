class Vec2{
	
	/**
	 * @param {Number} x 
	 * @param {Number} y 
	 */
	constructor(x = 0, y = x){
		this.x = x;
		this.y = y;
	}
	/**
	 * @type {Vec2} creates a vector from the specified angle, in radians
	 * @param {Number} angle the angle the vector will point in
	 * @param {Number} magnitude the magnitude of the new vector 
	 */
	static FromAngle(angle, magnitude = 1){ return new Vec2(Math.cos(angle) * magnitude, Math.sin(angle) * magnitude); }
	
	/**@type {Vec2}*/
	get clone(){
		return new Vec2(this.x, this.y);
	}
	/**@type {Vec2}*/
	static get up(){ return new Vec2(0, -1); }
	/**@type {Vec2}*/
	static get down(){ return new Vec2(0, 1); }
	/**@type {Vec2}*/
	static get left(){ return new Vec2(-1, 0); }
	/**@type {Vec2}*/
	static get right(){ return new Vec2(1, 0); }
	
	/**@type {Vec2} returns the negated vector*/
	get opposite(){ return new Vec2(-this.x, -this.y); }
	/**@type {Vec2} */
	get normalized(){ return this.Multiply(1 / this.magnitude); }
	/**@type {Number}*/
	get magnitude(){ return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2)); }
	/**@type {Number} returns the angular direction that this vector points to, in radians*/
	get direction(){ return Math.atan2(this.y, this.x); }

	/** @param {Vec2} vec */
	Add(vec){ return new Vec2(this.x + vec.x, this.y + vec.y); }
	/** @param {Vec2} vec */
	Subtract(vec){ return this.Add(vec.opposite); }
	/**
	 * @type {Number} returns the product of the vector multiplied by a specified factor
	 * @param {Number} factor 
	 */
	Multiply(factor){ return new Vec2(this.x * factor, this.y * factor); }
	/**
	 * @type {number} returns the dot product of this and another vector
	 * @param {Vec2} vecB 
	 */
	Dot(vecB){ return this.x * this.y + vecB.x * vecB.y; }
	/**
	 * @type {Vec2} scales the vector by another vector, by 
	 * multiplying their x and y components
	 * @param {Vec2} VecB 
	 */
	Scale(vecB){ return new Vec2(this.x * vecB.x, this.y * vecB.y); }
	/**
	 * @type {Vec2} returns this vector rotated around <0,0>
	 * by the specified amount, in radians
	 * @param {Number} rotation 
	 */
	Rotate(rotation){ return Vec2.FromAngle(this.direction + rotation, this.magnitude); }

	/** @param {Vec2} vec */
	Distance(vec){ return vec.Subtract(this).magnitude; }

	/**
	 * returns true if a and b have the exact same x and y values
	 * @param {Vec2} a 
	 * @param {Vec2} b 
	 */
	static AreEqual(a, b){
		return a.x == b.x && a.y == b.y;
	}
	/**
	 * returns a vector withe the maximum x value and the maximum y values of the two 
	 * vectors supplied
	 * @param {Vec2} a 
	 * @param {Vec2} b 
	 */
	static Max(a, b){ return new Vec2(Math.max(a.x, b.x), Math.max(a.y, b.y)); }
	/**
	 * returns a vector withe the minimum x value and the minimum y values of the two 
	 * vectors supplied
	 * @param {Vec2} a 
	 * @param {Vec2} b 
	 */
	static Min(a, b){ return new Vec2(Math.min(a.x, b.x), Math.min(a.y, b.y)); }
	/**
	 * @type {Boolean} determines whether or not 2 normalized vectors face each other
	 * @param {Vec2} normA 
	 * @param {Vec2} normB 
	 */
	static InwardFacing(normA, normB){
		return Math.abs(Vec2.Angle(normA, normB)) > Math.PI / 2;
	}
	/**
	 * @type {Number} returns the sortest signed angle from normA to normB
	 * @param {Vec2} normA 
	 * @param {Vec2} normB 
	 */
	static Angle(normA, normB){
		let dif = (normB.direction - normA.direction + Math.PI) % (Math.PI * 2) - Math.PI;
		return dif < -Math.PI ? dif + Math.PI * 2 : dif;
	}

	/**
	 * renders the point on the specified context
	 * @param {CanvasRenderingContext2D} ctx 
	 * @param {string} fillstyle 
	 * @param {Number} thickness 
	 */
	RenderPoint(ctx, fillstyle, thickness = 2){
		
		ctx.fillStyle = fillstyle;
		let rect = new Rect(this.Subtract(new Vec2(thickness * 0.5)), this.Add(new Vec2(thickness * 0.5)));
		rect.RenderFill(ctx, fillstyle);
	}

	ToString(){
		return "<" + this.x + "," + this.y + ">";
	}
}