class Rect{

	/**
	 * initializes an axis aligned bounding box from two vectors that represent
	 * the opposite corners of the box in world space
	 * @param {*} min 
	 * @param {*} max 
	 */
	constructor(min, max = min){

		/**@type {Vec2}*/
		this._min = Vec2.Min(min, max);
		/**@type {Vec2}*/
		this._max = Vec2.Max(min, max);
	}

	/**@type {Vec2} */
	get clone(){ return new Rect(this._min.clone, this._max.clone); }

	/**@type {Vec2} */
	get min(){ return this._min.clone; }
	set min(value){
		this._min = Vec2.Min(value, this._max);
		this._max = Vec2.Max(value, this._max);
	}
	/**@type {Vec2} */
	get max(){ return this._max.clone; }
	set max(value){
		this._max = Vec2.Max(value, this._min);
		this._min = Vec2.Min(value, this._min);
	}

	/**@type {Vec2} */
	get size(){ return this._max.Subtract(this.min); }
	/**@type {Number} */
	get width(){ return this.size.x; }
	/**@type {Number} */
	get height(){ return this.size.y; }
	/**@type {Number} */
	get area(){ return this.size.x * this.size.y; }
	/**@type {Boolean} */
	get isEmpty(){ return this._max.x == this.min.x || this.max.y == this.min.y; }

	/**@type {Number} */
	get left(){ return this._min.x; }
	/**@type {Number} */
	get right(){ return this._max.x; }
	/**@type {Number} */
	get top(){ return this._min.y; }
	/**@type {Number} */
	get bottom(){ return this._max.y; }

	/**@type {Vec2} */
	get topLeft(){ return this._min.clone; }
	/**@type {Vec2} */
	get topRight(){ return new Vec2(this._max.x, this._min.y); }
	/**@type {Vec2} */
	get bottomLeft(){ return new Vec2(this._min.x, this._max.y); }
	/**@type {Vec2} */
	get bottomRight(){ return this._max.clone; }
	/**@type {Vec2} */
	get center(){ return this._min.Add(this._max).Multiply(0.5); }

	/**
	 * Sets the center of the box to the specified point
	 * @param {Vec2} point 
	 */
	SetCenter(point){
		let dif = point.Subtract(this.center);
		this._min = this._min.Add(dif);
		this._max = this._max.Add(dif);
	}

	/**
	 * Moves the Rect by the specified delta
	 * @param {Vec2} delta 
	 */
	Translate(delta){ return new Rect(this._min.Add(delta), this._max.Add(delta)); }
	/**
	 * @type {Rect} returns a rect scaled by the specified amount
	 * @param {Vec2} delta 
	 */
	Scale(delta){
		let sp = this._min.Scale(delta);
		let ep = this._max.Scale(delta)
		return new Rect(sp, ep);
	}
	/**
	 * expands the Rect to contain the specified point
	 * @param {Vec2} point 
	 */
	Expand(point){

		if(point.x < this._min.x){
			this._min.x = point.x;
		}
		else if(point.x > this._max.x){
			this._max.x = point.x;
		}
		if(point.y < this._min.y){
			this._min.y = point.y;
		}
		else if(point.y > this._max.y){
			this._max.y = point.y;
		}
	}

	/**
	 * @type {Rect} returns the area of the two rects that are overlapping
	 * @param {Rect} rect 
	 */
	Intersection(rect){
		let max = Vec2.Min(this._max, rect.max);
		let min = Vec2.Max(this._min, rect._min);

		let dif = max.Subtract(min);
		if(dif.x < 0 || dif.y < 0){
			return null;
		}

		return new Rect(min, max);
	}
	/**
	 * @type {Boolean} low performance cost check to see if two rects are overlapping
	 * @param {Rect} rect 
	 */
	Overlaps(rect){
		return (
			this._min.x < rect._max.x &&
			this._min.y < rect._max.y &&
			this._max.x > rect.min.x &&
			this._max.y > rect.min.y 
		);
	}
	/**
	 * @type {Boolean}
	 * @see {Overlaps} same as 'Overlaps()' except it returns positive for edge equality also
	 * @param {Rect} rect 
	 */
	OverlapsInclusive(rect){
		return (
			this._min.x <= rect._max.x &&
			this._min.y <= rect._max.y &&
			this._max.x >= rect.min.x &&
			this._max.y >= rect.min.y 
		);
	}
	/**
	 * @type {Boolean} returns true if the point lies within this rect(inclusive)
	 * @param {Vec2} point 
	 */
	ContainsPoint(point){ 
		return (
			this._min.x <= point.x &&
			this._max.x >= point.x &&
			this._min.y <= point.y &&
			this._max.y >= point.y
		); 
	}
	/**
	 * @type {Boolean} returns true if the rect lies completely within this rect(inclusive)
	 * @param {Rect} rect 
	 */
	ContainsRect(rect){
		return (
			this._min.x <= rect._min.x &&
			this._max.x >= rect._max.x &&
			this._min.y <= rect._min.y &&
			this._max.y >= rect._max.y
		);
	}

	/**
	 * @param {CanvasRenderingContext2D} ctx 
	 * @param {string} fillstyle 
	 */
	RenderFill(ctx, fillstyle){
		
		let size = this.size;
		ctx.fillStyle = fillstyle;
		ctx.fillRect(this._min.x, this._min.y, size.x, size.y);
	}
	/**
	 * @param {CanvasRenderingContext2D} ctx 
	 * @param {string} strokeStyle 
	 * @param {Number} thickness
	 */
	RenderOutline(ctx, strokeStyle, thickness){

		ctx.strokeStyle = strokeStyle;
		ctx.lineWidth = thickness;

		ctx.beginPath();
		ctx.moveTo(this.left, this.top);
		ctx.lineTo(this.right, this.top);
		ctx.lineTo(this.right, this.bottom);
		ctx.lineTo(this.left, this.bottom);
		ctx.closePath();
		ctx.stroke();
	}
}