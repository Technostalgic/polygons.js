class Composite{

	/**
	 * @param {Array<Polygon>} polys 
	 */
	constructor(polys){

		/**@type {Array<Polygon>} */
		this._polys = polys;
	}

	/**
	 * Renders the composite's outline
	 * @param {CanvasRenderingContext2D} ctx 
	 * @param {String} strokeStyle 
	 * @param {Number} thickness 
	 */
	RenderOutline(ctx, strokeStyle, thickness){

		for(let i = this._polys.length - 1; i >= 0; i--){
			this._polys[i].RenderOutline(ctx, strokeStyle, thickness);
			this._polys[i]._boundingBox.RenderOutline(ctx, "#00A", 1)
		}
	}

	/**
	 * Renders the composite's filled area
	 * @param {CanvasRenderingContext2D} ctx 
	 * @param {String} fillStyle 
	 */
	RenderFill(ctx, fillStyle){

		for(let i = this._polys.length - 1; i >= 0; i--){
			this._polys[i].RenderFill(ctx, fillStyle);
		}
	}
}