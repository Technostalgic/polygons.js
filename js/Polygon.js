class Polygon{

	constructor(verts = []){

		/**@type {Array<Vec2>} the polygon's local vertices, call 'MarkDirty()' after any modifications*/
		this._verts = verts;

		/**@type {Array<Ray2>} private - do not modify */
		this.__edges = null;

		/**@type {Rect} private - do not modify */
		this.__boundingBox = null;

		/**@type {Vec2} private - do not modify */
		this.__vertexAverage = null;
	}
	/**
	 * @type {Polygon} creates a circular polygon
	 * @param {Vec2} center
	 * @param {Number} vertices the amount of vertices the circle has 
	 * @param {Number} radius 
	 */
	static CreateCircle(center, vertices = 12, radius = 25){

		let verts = [];
		for(let i = 0; i < vertices; i++){
			let ang = i / vertices * Math.PI * 2;
			verts.push(Vec2.FromAngle(ang, radius).Add(center));
		}

		return new Polygon(verts);
	}

	/**@type {Polygon} */
	get clone(){
		let r = new Polygon();

		for(let i = 0; i < this._verts.length; i++){
			r._verts.push(this._verts[i].clone);
		}
		
		return r;
	}

	/**
	 * @type {Array<Vec2>} getter for the polygon's absolute vertices,
	 * reference type - do not modify 
	 * */
	get _vertices(){
		return this._verts;
	}

	/**
	 * @type {Array<Ray2>} getter for the polygon's absolute edges,
	 * reference type - do not modify 
	*/
	get _edges(){
		if(this.__edges == null) this._calcEdges();
		return this.__edges;
	}
	_calcEdges(){
		/**
		 * clear edges
		 * for each vertex
		 *  add a ray created from this vertex to the next vertex
		 */
		this.__edges = [];
		for(let i = 0; i < this._verts.length; i++){
			let i2 = (i + 1) % this._verts.length;
			this.__edges.push(Ray2.FromPoints(this._verts[i], this._verts[i2]));
		}
	}

	/**
	 * @type {Vec2} 
	 * @param {Number} index the index of the normal's corresponding edge
	*/
	GetNormal(index){
		
		let sp = this._vertices[index];
		let i2 = (index + 1) % this._verts.length;
		let ep = this._vertices[i2];
		let dif = ep.Subtract(sp);
		return Vec2.FromAngle(dif.direction - Math.PI / 2);
	}

	/**
	 * @type {Rect} getter for the polygon's absolute bounding box,
	 * reference type - do not modify
	 */
	get _boundingBox(){
		if(this.__boundingBox == null) this._calcBoundingBox();
		return this.__boundingBox;
	}
	/**@type {Rect} de-referenced getter for _boundingBox */
	get boundingBox(){ return this._boundingBox.clone; }
	_calcBoundingBox(){

		// if there are no verts there is no bounding box either
		if(this._verts.length <= 0){
			this.__boundingBox = null;
			return;
		}		

		/**
		 * reset bounding box
		 * for each vertex
		 *  expand the bounding box to fit the vertex
		 */
		this.__boundingBox = new Rect(this._vertices[0]);
		for(let i = this._vertices.length - 1; i >= 0; i--){
			this.__boundingBox.Expand(this._vertices[i]);
		}
	}

	/**@type {Vec2} the average point of each vertex */
	get vertexAverage(){
		if(this.__vertexAverage == null) this._calcVertexAverage();
		return this.__vertexAverage;
	}
	_calcVertexAverage(){

		let r = new Vec2();
		for(let i = this._vertices.length - 1; i >= 0; i--){
			r = r.Add(this._vertices[i]);
		}
		this.__vertexAverage = r.Multiply(1 / this._vertices.length);
	}

	/**
	 * Marks the polygon as dirty so that all dynamic properties get
	 * recalculated when they are retrieved
	 */
	MarkDirty(){
		this.__edges = null;
		this.__boundingBox = null;
		this.__vertexAverage = null;
	}

	/**
	 * translates (changes position of) the polygon by the 
	 * specified delta vector
	 * @param {Vec2} delta 
	 */
	Translate(delta){
		for(let i = this._verts.length - 1; i >= 0; i--){
			let point = this._verts[i].Add(delta);
			this._verts[i] = point;
			if(this.__edges != null)
				this.__edges[i]._origin = point;
		}
		if(this._boundingBox != null) this._boundingBox.SetCenter(this._boundingBox.center.Add(delta));
		if(this.__vertexAverage != null) this.__vertexAverage = this.__vertexAverage.Add(delta);
	}
	/**
	 * rotates the polygon by the specified factor
	 * @param {Number} delta 
	 */
	Rotate(delta){
		for(let i = this._verts.length - 1; i >= 0; i--){
			let point = this._verts[i].Rotate(delta);
			this._verts[i] = point;
			if(this.__edges != null)
				this.__edges[i] = new Ray2(
					this.__edges[i]._origin.Rotate(delta), 
					this.__edges[i]._directionNormal.Rotate(delta), 
					this.__edges[i].length);
		}
		if(this._boundingBox != null && delta != 0) this._boundingBox = null;
		if(this.__vertexAverage != null) this.__vertexAverage = this.__vertexAverage.Rotate(delta);
	}
	/**
	 * scales the polygon by the specified factor
	 * @param {Vec2} delta 
	 */
	Scale(delta){
		for(let i = this._verts.length - 1; i >= 0; i--){
			let point = this._verts[i].Scale(delta);
			this._verts[i] = point;
			if(this.__edges != null)
				this.__edges[i] = this.__edges[i].Scale(delta);
		}
		if(this._boundingBox != null) this._boundingBox = this._boundingBox.Scale(delta);
		if(this.__vertexAverage != null) this.__vertexAverage = this.__vertexAverage.Scale(delta);
	}
	/**
	 * transforms the polygon by the specified transform delta
	 * @param {Transform} delta 
	 */
	Transform(delta){
		this._verts = delta.TransformPoints(this._verts);
		if(this._boundingBox != null) {
			if(delta.rotation != 0){
				this._boundingBox = null;
			}
			else{
				let bb = this._boundingBox.Scale(delta._scale);
				bb.SetCenter(bb.center.Add(delta._translation));
				this._boundingBox = bb;
			}
		}
		if(this.__vertexAverage != null){
			this.__vertexAverage = delta.TransformPoint(this.__vertexAverage);
		}
	}
	
	/**
	 * @type {DynamicPolygon} forces the polygon to be returned as a Dynamic Polygon
	 * @param {Boolean} reposition whether or not the new 
	 *  dynamic polygon should recenter it's verts around
	 *  <0,0> and have the dynamic polygon's new transform
	 *  field adjust for the resulting disposition
	 */
	ForceToDynamic(reposition = false){

		let r = new DynamicPolygon();
		
		let ova = this.vertexAverage.clone;
		if(reposition){
			this.Translate(ova.Multiply(-1));
		}

		Object.assign(r, this.clone);

		if(reposition){
			this.Translate(ova);
			r.SetVertexOrigin(this.vertexAverage);
			r.Translate(ova);
			r.MarkDirty();
		}

		return r;
	}
	
	/**
	 * @type {Boolean} checks to see if the specified point is inside the polygon
	 * @param {Vec2} point 
	 */
	ContainsPoint(point){

		// if it's not inside the bounding box, it's not inside the polygon
		if(!this._boundingBox.ContainsPoint(point)) return false;

		let isinside = false;
		let test = new Ray2(point, Vec2.right, this._boundingBox.width);
		for(i = this._edges.length - 1; i >= 0; i--){
			if(test.Intersection(this._edges[i])){
				isinside = !isinside;
			}
		}

		return isinside;
	}

	/**
	 * renders the normal vectors of the polygon's edges
	 * @param {CanvasRenderingContext2D} ctx 
	 * @param {string} fillStyle 
	 * @param {Number} dist 
	 */
	RenderNormals(ctx, fillStyle, dist){
		for(let i = this._verts.length - 1; i >= 0; i--){
			let point = this._edges[i].boundingBox.center;
			let norm = this.GetNormal(i);
			point.RenderPoint(ctx, fillStyle, 3);
			point.Add(norm.Multiply(dist / 2)).RenderPoint(ctx, fillStyle, 2);
			point.Add(norm.Multiply(dist)).RenderPoint(ctx, fillStyle, 1);
		}
	}
	/**
	 * Renders the outline of the polygon onto the specified canvas context
	 * @param {CanvasRenderingContext2D} ctx 
	 * @param {string} strokestyle 
	 * @param {Number} thickness 
	 */
	RenderOutline(ctx, strokestyle, thickness){

		// no verts = nothing to render
		if(this._vertices.length <= 0) return;

		let startpos = this._vertices[this._vertices.length - 1];

		ctx.strokeStyle = strokestyle;
		ctx.lineWidth = thickness;

		ctx.beginPath();
		ctx.moveTo(startpos.x, startpos.y);
		for(var i = this._vertices.length - 1; i >= 0; i--){
			ctx.lineTo(this._vertices[i].x, this._vertices[i].y);
		}
		ctx.closePath();
		ctx.stroke();
	}
	/**
	 * Renders the fill of the polygon onto the specified canvas context
	 * @param {CanvasRenderingContext2D} ctx 
	 * @param {string} fillStyle 
	 */
	RenderFill(ctx, fillStyle){

		// no verts = nothing to render
		if(this._vertices.length <= 0) return;

		ctx.fillStyle = fillStyle;

		ctx.beginPath();
		let startVert = this._vertices[this._vertices.length - 1];
		ctx.moveTo(startVert.x, startVert.y);
		for(var i = this._vertices.length - 1; i >= 0; i--){
			ctx.lineTo(this._vertices[i].x, this._vertices[i].y);
		}
		ctx.closePath();
		ctx.fill();
	}
}