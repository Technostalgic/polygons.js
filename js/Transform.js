class Transform{

	/**
	 * @type {Transform} creates a new Transform instance
	 * @param {Vec2} translate 
	 * @param {Number} rotate 
	 * @param {Vec2} scale 
	 */
	constructor(translate = new Vec2(), rotate = 0, scale = new Vec2(1)){
		
		/**@type {Vec2} */
		this._translation = translate;
		
		/**@type {Number} */
		this.rotation = rotate;

		/**@type {Vec2} */
		this._scale = scale;
	}

	/**@type {Transform} */
	get clone(){

		let r = new Transform();
		r._translation = this._translation.clone;
		r.rotation = this.rotation;
		r._scale = this._scale.clone;

		return r;
	}

	/** @type {Vec2} de-referenced getter/setter for _translation */
	get translation(){ return this._translation.clone; }
	set translation(val){ this._translation = val; }
	
	/** @type {Vec2} de-referenced getter/setter for _scale */
	get scale(){ return this._scale.clone; }
	set scale(val){ this._scale = val; }

	/**
	 * @type {Transform} combines the two transforms and returns the result
	 * @param {Transform} transform 
	 */
	Add(transform){

		let r = this.clone;
		r.scale = r.scale.Scale(transform._scale);
		r.rotation = r.rotation + transform.rotation;
		r.translation = r.translation.Add(transform._translation);

		return r;
	}
	/**
	 * @type {Transform} takes the difference of the specified transform from
	 * this transform and returns the difference
	 * @param {Transform} transform 
	 */
	Subtract(transform){

		let r = this.clone;
		r._translation = r._translation.Subtract(transform._translation);
		r.rotation = r.rotation - transform.rotation;
		r._scale = r._scale.Scale(new Vec2(1 / transform._scale.x, 1 / transform._scale.y));

		return r;
	}

	/**
	 * @type {Vec2} returns a point transformed by this transform's
	 * tranformation properties: translation, rotation, and scale
	 * @param {Vec2} point 
	 */
	TransformPoint(point){

		let r = point.clone;
		r = r.Scale(this._scale);
		r = r.Rotate(this.rotation);
		r = r.Add(this._translation);

		return r;
	}

	/**
	 * @type {Array<Vec2>} transforms all the points in the spcified
	 * array and returns them
	 * @param {Array<Vec2>} points 
	 */
	TransformPoints(points){
		
		let r = [];
		for(let i = 0; i < points.length; i++){
			r.push(this.TransformPoint(points[i]));
		}
		return r;
	}

	/**
	 * @type {Ray2}
	 * @param {Ray2} ray 
	 */
	TransformRay(ray){

		/**
		 * if the scale has a 1:1 aspect ratio, we can optomize the
		 * function by cutting out one of the TranformPoint calls, and 
		 * some unnecessary Trigonometric calculations that appear inside 
		 * of Ray2.FromPoints
		 */
		if(this._scale.x == this._scale.y){
			return new Ray2(this.TransformPoint(ray._origin), ray._directionNormal.Rotate(this.rotation), ray.length * this._scale.x);
		}

		let sp = this.TransformPoint(ray._origin);
		let ep = this.TransformPoint(ray.endPoint);
		let r = Ray2.FromPoints(sp, ep);

		return r;
	}

	/**
	 * @type {Vec2} returns the a point that represents the specified 
	 * point's original value before being transformed by this 
	 * Transform object
	 * @param {Vec2} point 
	 */
	ReverseTransformPoint(point){

		let r = point.clone;
		r = r.Subtract(this._translation);
		r = r.Rotate(-this.rotation);
		r = r.Scale(new Vec2(1 / this._scale.x, 1 / this._scale.y));

		return r;
	}
}