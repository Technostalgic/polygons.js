class BooleanOperation{

	/**
	 * @public @type {Composite} Unifies two composites or polygons and returns the resulting composite
	 * @param {Composite} a the shape that will be appended to, can also be of type Polygon
	 * @param {Composite} b the shape that will be appended onto shape a, can also be of type Polygon
	 */
	static Union(a, b){
		
		/** ensure a and b are converted to @see Composite types if they are not */
		if(a instanceof Polygon) a = new Composite([a]);
		if(b instanceof Polygon) b = new Composite([b]);

		/**
		 * usedPolyBIndices - an array to store which polygons in composite B are unified
		 * polys - an array to hold all the polygons that the resulting composite will consist of
		 */
		let usedPolyBIndices = [];
		let polys = [];

		/** iterate through all polygons of composite a & b */
		for(let i1 = a._polys.length - 1; i1 >= 0; i1--){
			let polyA = a._polys[i1];

			/** a variable track whether or not polyA is unified while iterating through composite b */
			let polyAUsed = false;

			/** iterate through all polygons in composite B */
			for(let i2 = b._polys.length - 1; i2 >= 0; i2--){
				let polyB = b._polys[i2];

				/** let polyC be a unification of polys A and B */
				let polyC = this._calcUnionPoly(polyA, polyB);

				/** if polyC doesn't exist, do nothing */
				if(polyC == null){
					continue;
				}

				/** if polyC exists, notify that polyA was unified and add polyC to the output */
				polyAUsed = true;
				polys.push(polyC);

				/** 
				 * push the poly refs to the used poly ref list to indicate they were used in the 
				 * unification 
				 */
				if(!usedPolyBIndices.includes(polyB)){
					usedPolyBIndices.push(i2);
				}
			}

			/** if polyA was not unified with another polygon, added it to the output composite*/
			if(!polyAUsed){
				polys.push(polyA.clone);
			}
		}

		/**
		 * iterate through each polygon in composite b and if they were not unified, add them as 
		 * standalone polygons to the resulting composite
		 */
		for(let i = b._polys.length - 1; i >= 0; i--){
			if(!usedPolyBIndices.includes(i)){
				polys.push(b._polys[i].clone);
			}
		}
		
		// TODO fix case where unioning more than 2 polygons (connected together by 1) at once doesn't work
		/** create a new composite from the polygon list and make sure it has no overlapping polygons */
		let r = this._selfUnion(new Composite(polys));

		return r;
	}

	/** TODO optomize both this function and Union(a,b) with respect to this function
	 * if a composite has any intersecting polygons inside itself, this method unifies them into
	 * the same polygon, reducing the overall number of polygons
	 * @param {Composite} a the composite to perform the polygon union on
	 */
	static _selfUnion(a){
		
		/** if a has only one polygon, do nothing */
		if(a._polys.length <= 1){
			return a;
		}
		
		// TODO fix infinite recursion that frequently happens in this function

		/**
		 * usedPolyBIndices - an array to store which polygons in composite B are unified
		 * polys - an array to hold all the polygons that the resulting composite will consist of
		 */
		let usedPolyIndices = [];
		let polys = [];

		/** iterate through all polygons of composite a & b */
		for(let i1 = a._polys.length - 1; i1 >= 0; i1--){
			let polyA = a._polys[i1];

			/** 
			 * iterate through all polygons in composite A that haven't already been tested for 
			 * intersection against PolyA 
			 */
			for(let i2 = i1 - 1; i2 >= 0; i2--){
				let polyB = a._polys[i2];

				/** let polyC be a unification of polys A and B */
				let polyC = this._calcUnionPoly(polyA, polyB);

				/** if polyC doesn't exist, do nothing */
				if(polyC == null){
					continue;
				}

				/** if polyC exists, notify that polyA was unified and add polyC to the output */
				usedPolyIndices.push(i1, i2);
				polys.push(polyC);
			}

			/** if polyA was not unified with another polygon, added it to the output composite*/
			if(!usedPolyIndices.includes(i1)){
				polys.push(polyA.clone);
			}
		}
		
		return new Composite(polys);
	}

	/**
	 * @private unifies 2 polygons into a single one, or returns null if they do not intersect
	 * @param {Polygon} polyA 
	 * @param {Polygon} polyB 
	 */
	static _calcUnionPoly(polyA, polyB){

		if(!PolygonCollision.DoPolygonsIntersect(polyA, polyB))
			return null;

		let verts = [];
		this._unionEdgeCrawl(polyA._edges[0], 0, polyA, polyB, verts, false);

		return new Polygon(verts);
	}

	/** 
	 * @private starts on the specified edge of a polygon and "crawls" around the crawlPoly clockwise
	 * until it intersects with intersectPoly, then crawls around the edges of intersectPoly until it
	 * intersects back with crawlPoly again. It continues to crawl around the polygon's edges until it
	 * reaches the first intersection point again, all the while adding the vertices of the polygons
	 * that it's "crawling around" to the outVerts array.
	 * @param {Ray2} edge the edge that the crawl starts on
	 * @param {Number} index the index of the edge on the crawlPoly
	 * @param {Polygon} crawlPoly the poly that is being crawled around
	 * @param {Polygon} intersectPoly the poly that is being tested for intersection
	 * @param {Array<Vec2>} outVerts the vertex array that is appended to
	 * @param {Boolean} pushFirst whether or not the vertex array will be appended to
	 */
	static _unionEdgeCrawl(edge, index, crawlPoly, intersectPoly, outVerts, pushFirst){

		for(let e1 = 0; e1 < crawlPoly._edges.length + 1; e1++){
			let edgeA = edge != null ? edge : crawlPoly._edges[index];

			/** return if back at starting vertex */
			if(outVerts.length > 0){
				if(Vec2.AreEqual(outVerts[0], edgeA._origin)){
					console.log("vertex crawl finished with " + outVerts.length + " verts");
					return;
				}

				/** push the vertex to the array if the outVerts array has already been started */
				outVerts.push(edgeA.origin);
			}
			
			/** 
			 * if the "crawl" has not been started, don't append to output array unless explicitly 
			 * stated to do so 
			 */
			else if(pushFirst){
				outVerts.push(edgeA.origin);
				console.log("Vertex crawl Began");
			}

			/** 
			 * if there is an intersection on the current edge, jump to the intersectPoly and start
			 * crawling around it
			 */
			let colA = PolygonCollision.CalculateClosestRayCollision(edgeA, intersectPoly, true, true);
			if(colA != null){

				
				/** 
				 * define a starting edge that begins at the intersection point and ends at the end
				 * of the edge that was collided with
				 */
				let startEdge = Ray2.FromPoints(colA._intersectPoint, colA._rayHit.endPoint);
				
				/** 
				 * recursively call this function to hop between the crawlPoly and intersectPoly as
				 * many times as they intersect
				 */
				console.log("Vertex crawl tranfer to intersect poly at edgeA #" + index + " and edgeB #" + colA._polygonHitVertex);
				this._unionEdgeCrawl(startEdge, colA._polygonHitVertex, intersectPoly, crawlPoly, outVerts, true);
				break;
			}
			
			edge = null;
			index = (index + 1) % crawlPoly._edges.length;
		}
	}
}