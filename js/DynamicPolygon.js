class DynamicPolygon extends Polygon{

	constructor(verts = [], transform = new Transform()){
		super(verts);

		/**@type {Array<Vec2>} private - do not modify */
		this.__transformedVerts = null;

		/**@type {Array<Ray2>} private - do not modify */
		this.__transformedEdges = null;

		/**@type {Transform} private - do not modify */
		this.__transform = transform;
	}

	/**@type {DynamicPolygon} */
	get clone(){

		var r = new DynamicPolygon();
		Object.assign(r, super.clone);
		
		r.__transform = this.__transform.clone;

		return r;
	}

	/**
	 * @type {Transform} getter/setter for the polygon's transform
	 * data, reference type - do not modify
	 */
	get _transform(){ return this.__transform; }
	set _transform(val){
		if(this.__vertexAverage != null){ 
			this.__vertexAverage = this.__transform.ReverseTransformPoint(this.__vertexAverage);
			this.__vertexAverage = val.TransformPoint(this.__vertexAverage);
		}
		if(this._boundingBox != null){ 
			/**
			 * bounding boxes can only be transformed if there is no rotation
			 * involved, so if the new transform's rotation is not equal to
			 * the previous transform's rotation, the bounding box is just marked
			 * dirty so it is recalculated when needed
			 */
			if(this.__transform.rotation == val.rotation){
				let sp = this._boundingBox._min;
				let ep = this._boundingBox._max;
				sp = this.__transform.ReverseTransformPoint(sp);
				ep = this.__transform.ReverseTransformPoint(ep);
				this._boundingBox = new Rect(sp, ep);
			}
			else{
				this._boundingBox == null;
			}
		}
		this.__transform = val;
	}

	/** @type {Transform} de-referenced getter/setter for _transform */
	get transform(){ this._transform.clone; }
	set transform(val){ this._transform = val; }

	/**
	 * @type {Array<Ray2>} getter for the polygon's absolute edges,
	 * reference type - do not modify 
	*/
	get _edges(){
		if(this.__transformedEdges == null)
			this._calcTransformedEdges();
		return this.__transformedEdges;
	}
	_calcTransformedEdges(){
		
		if(this.__edges == null)
			this._calcEdges();
			
		this.__transformedEdges = [];
		for(let i = 0; i < this.__edges.length; i++){
			this.__transformedEdges.push(this._transform.TransformRay(this.__edges[i]));
		}
	}

	/**@type {Vec2} */
	GetNormal(index){
		let r = super.GetNormal(index);
		return r.Rotate(this.__transform.rotation);
	}

	/**
	 * @type {Array<Vec2>} getter for the polygon's absolute vertices,
	 * reference type - do not modify 
	 * */
	get _vertices(){
		if(this.__transformedVerts == null)
			this._calcTransformedVertices();
		return this.__transformedVerts;
	}
	_calcTransformedVertices(){

		// uses this._transform to transform all of the _verts
		this.__transformedVerts = [];
		for(let i = 0; i < this._verts.length; i++){
			this.__transformedVerts.push(this.__transform.TransformPoint(this._verts[i]));
		}
	}

	/**
	 * Marks the polygon as dirty so that all dynamic properties get
	 * recalculated when they are retrieved
	 */
	MarkDirty(){
		super.MarkDirty();
		this.__transformedVerts = null;
		this.__transformedEdges = null;
	}

	/**
	 * sets the origin point that the local vertices' position
	 * is based around
	 * @param {Vec2} point 
	 */
	SetVertexOrigin(point){
		for(let i = this._verts.length - 1; i >= 0; i--){
			this._vertices[i] = this._vertices[i].Subtract(point);
		}
	}
	Translate(delta){
		this.__transform._translation = this.__transform._translation.Add(delta);

		this.__transformedVerts = null;
		if(this._boundingBox != null)
			this.__boundingBox = this.__boundingBox.Translate(delta);
		if(this.__vertexAverage != null)
			this.__vertexAverage = this.__vertexAverage.Add(delta);
		this.__transformedEdges = null;
	}
	Rotate(delta){
		this.__transform.rotation = this.__transform.rotation + delta;

		this.__transformedVerts = null;
		this.__boundingBox = null;
		this.__vertexAverage = null;
		this.__transformedEdges = null;
	}
	Scale(delta){
		this.__transform._scale = this.__transform._scale.Scale(delta);

		this.__vertexAverage = null;
		this.__transformedVerts = null;
		this.__transformedEdges = null;
	}
	Transform(delta){
		this.__transform = this.__transform.Add(delta);

		this.__transformedVerts = null;
		this.__boundingBox = null;
		this.__vertexAverage = null;
		this.__transformedEdges = null;
	}

	/**@type {DynamicPolygon} returns a reference to itself */
	ForceToDynamic(reposition = false){
		return this;
	}
}