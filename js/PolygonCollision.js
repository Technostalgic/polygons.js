class PolygonCollision{

	constructor(){

		/**@type {Ray2} the ray that was casted, reference type - do not modify */
		this._rayCasted = null;
		/**@type {Ray2} the ray that was hit, reference type - do not modify */
		this._rayHit = null;

		/**@type {Polygon} the polygon that was casted, reference type - be careful with modifying */
		this._polygonCasted = null;
		/**@type {Polygon} the polygon that was hit, reference type - be careful with modifying */
		this._polygonHit = null;

		// TODO: firgure out if this is actually needed or not
		 /**@type {Number} the index of the casted polygon's vertex edge that collided */
		 this._polygonCastedVertex = null;
		 /**@type {Number} the index of the hit polygon's vertex edge that collided */
		 this._polygonHitVertex = null;

		/**@type {Vec2} a reference to the intersection point where the rays collided */
		this._intersectPoint = null;
		/**@type {Vec2} a reference the normal vector of the ray that was hit */
		this._normal = null;
	}

	/**
	 * @type {Array<PolygonCollision>} returns a list of 
	 * collision pointes where the specified polygon
	 * intersects itself
	 * @param {Polygon} poly
	 * @param {Polygon} backfaceCull whether or not both sides of each 
	 *  edge should be tested against, or just the side with a 
	 *  positive normal vector
	 */
	static CalculateSelfIntersections(poly, backfaceCull = true){

		let r = [];
		for(let ce = poly._edges.length - 1; ce >= 0; ce--){
			for(let re = poly._edges.length - 1; re >= 0; re--){
				
				/**
				 * skip the raycast test if it's the same 
				 * edge, or the one before / after it
				 */
				let e0 = ce - 1;
				if(e0 < 0) e0 += poly._edges.length;
				let e2 = (ce + 1) % poly._edges.length;
				if(re === e0 || re === ce || re === e2){
					continue;
				}

				if(backfaceCull){
					if(!Vec2.InwardFacing(poly._edges[ce].directionNormal, poly.GetNormal(re)))
						continue;
				}

				let intersect = Ray2.RayIntersect(poly._edges[ce], poly._edges[re]);
				if(intersect == null){
					continue;
				}

				let col = new PolygonCollision();
				col._intersectPoint = intersect;
				col._rayCasted = poly._edges[ce];
				col._rayHit = poly._edges[re];
				col._polygonCasted = poly;
				col._polygonHit = poly;
				col._normal = poly.GetNormal(re);
				r.push(col);
			}
		}
		return r;
	}
	/**
	 * @type {Boolean} returns true if the specified polygon's overlap at all
	 * @param {Polygon} polyA 
	 * @param {Polygon} polyB 
	 */
	static DoPolygonsIntersect(polyA, polyB){
		return this.CalculatePolygonCollisions(polyA, polyB).length > 0;
	}

	/**
	 * @type {Array<PolygonCollision>} calculates all the collisions between the ray and polygon
	 * @param {Ray2} ray the ray to cast
	 * @param {Polygon} poly the polygon to check against the ray
	 * @param {Boolean} backfaceCull whether or not the raycasts hit the reverse side of polygon edges,
	 *  I.E. just the entry point vs the entry point and exit point of the raycast 
	 */
	static CalculateRayCollisions(ray, poly, backfaceCull = true){

		if(!ray.IntersectsRect(poly._boundingBox)){
			return [];
		}

		let r = [];
		for(let i = poly._vertices.length - 1; i >= 0; i--){
			
			let edge = poly._edges[i];
			if(backfaceCull){
				if(!Vec2.InwardFacing(ray.directionNormal, poly.GetNormal(i)))
					continue;
			}

			let point = Ray2.RayIntersect(ray, edge);
			if(point == null){
				continue;
			}

			let col = new PolygonCollision();
			col._rayCasted = ray;
			col._rayHit = edge;
			col._polygonHit = poly;
			col._polygonHitVertex = i;
			col._intersectPoint = point;
			col._normal = poly.GetNormal(i);

			r.push(col);
		}

		return r;
	}
	/**
	 * @type {Vec2} returns the collision on the specified polygon that is closest
	 *  to the ray's origin
	 * @param {Ray2} ray 
	 * @param {Polygon} poly 
	 * @param {Boolean} backfaceCull 
	 * @param {Boolean} excludeStart 
	 */
	static CalculateClosestRayCollision(ray, poly, backfaceCull = true, excludeStart = false){

		/** get all the collision points */
		let cols = this.CalculateRayCollisions(ray, poly, backfaceCull);

		/** if no collisions, return null */
		if(cols.length <= 0) return null;

		/** 
		 * sort by distance from ray's origin and return the first PolygonCollision
		 * in the sorted list
		 */
		cols.sort(function(a, b){
			return ray._origin.Distance(a._intersectPoint) - ray._origin.Distance(b._intersectPoint);
		});

		/**
		 * if specified, ensure that the ray's origin point and the ray-polygon intersection
		 * point do not have the same value 
		 */
		if(excludeStart){

			let i = 0;
			while(Vec2.AreEqual(cols[i]._intersectPoint, ray._origin)){

				i++;
				if(i >= cols.length){
					return null;
				}
			}
			return cols[i];
		}

		return cols[0];
	}
	/**
	 * @type {Array<PolygonCollision>} calculates all the collisions between two polygons
	 * @param {Polygon} polyA 
	 * @param {Polygon} polyB 
	 * @param {Boolean} backfaceCull whether or not the raycasts hit the reverse side of polygon edges,
	 *  Note: if turned off there will only be one intersection point between two polygons per contact,
	 *  instead of having an entry and exit point, there will only be an entry point
	 */
	static CalculatePolygonCollisions(polyA, polyB, backfaceCull = false){

		if(!polyA._boundingBox.Overlaps(polyB._boundingBox)){
			return [];
		}

		let r = [];
		for(let iA = polyA._vertices.length - 1; iA >= 0; iA--){

			let edgeA = polyA._edges[iA];
			if(!edgeA.IntersectsRect(polyB._boundingBox)){
				continue;
			}
			
			for(let iB = polyB._vertices.length - 1; iB >= 0; iB--){

				let edgeB = polyB._edges[iB];
				if(backfaceCull){
					if(!Vec2.InwardFacing(edgeA.directionNormal, polyB.GetNormal(iB)))
						continue;
				}

				let point = Ray2.RayIntersect(edgeA, edgeB);
				if(point == null){
					continue;
				}

				let col = new PolygonCollision();
				col._rayCasted = edgeA;
				col._rayHit = edgeB;
				col._polygonCasted = polyA;
				col._polygonCastedVertex = iA;
				col._polygonHit = polyB;
				col._polygonHitVertex = iB;
				col._intersectPoint = point;
				col._normal = polyB.GetNormal(iB);

				r.push(col);
			}
		}

		return r;
	}

	/**
	 * @type {Array<PolygonCollision>} returns all the collisions between a ray and a set of polygons
	 * @param {Ray2} ray 
	 * @param {Array<Polygon>} polys 
	 * @param {Boolean} backfaceCull whether or not the raycasts hit the reverse side of polygon edges,
	 *  I.E. just the entry point vs the entry point and exit point of the raycast 
	 */
	static CalculateRayCollisionsInArray(ray, polys, backfaceCull = true){
		let r = [];
		for(let i = polys.length - 1; i >= 0; i--){
			r = r.concat(PolygonCollision.CalculateRayCollisions(ray, polys[i], backfaceCull));
		}
		return r;
	}
	/**
	 * @type {Array<PolygonCollision>} returns all the collisions between a polygon and a set of polygons
	 * @param {Polygon} poly 
	 * @param {Array<Polygon>} polys 
	 * @param {Boolean} backfaceCull whether or not the raycasts hit the reverse side of polygon edges,
	 *  Note: if turned off there will only be one intersection point between two polygons per contact,
	 *  instead of having an entry and exit point, there will only be an entry point
	 */
	static CalculatePolygonCollisionsInArray(poly, polys, backfaceCull = false){
		let r = [];
		for(let i = polys.length - 1; i >= 0; i--){
			r = r.concat(PolygonCollision.CalculatePolygonCollisions(poly, polys[i], backfaceCull));
		}
		return r;
	}
}