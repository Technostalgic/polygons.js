/**@type {HTMLCanvasElement} */
var canvas = document.getElementById("exCanvas");
/**@type {CanvasRenderingContext2D} */
var ctx = canvas.getContext("2d");

/**@type {Array<Polygon>} */
var polys = [];

/**@type {DynamicPolygon} */
var mainPoly = null;

function Init(){
	ClearScreen();

	GeneratePolygons(50);
	RenderPolygons();

	canvas.addEventListener("mousemove", OnMouseMove);
}

function GeneratePolygons(count = 10){

	for(let i = count; i > 0; i--){
		
		let size = Math.random() * 10 + 10;
		let verts = Math.random() * 10 + 3;
		let pos = new Vec2(Math.random() * canvas.width, Math.random() * canvas.height);

		polys.push(Polygon.CreateCircle(pos, verts, size));
	}
}

function GenerateMainPolygon(vec){
	
	mainPoly = Polygon.CreateCircle(new Vec2(), 8, 25).ForceToDynamic();
	mainPoly.Translate(vec);
}

function ClearScreen(){
	ctx.fillStyle = "#fff";
	ctx.fillRect(0, 0, canvas.width, canvas.height);
}

function RenderPolygons(){

	for(let i = polys.length - 1; i >= 0; i--){
		polys[i].RenderFill(ctx, "#faa");
	}
}

function RenderPolygonCollisions(){
	let cols = PolygonCollision.CalculatePolygonCollisionsInArray(mainPoly, polys, false);
	for(let i = cols.length - 1; i >= 0; i--){
		cols[i]._intersectPoint.RenderPoint(ctx, "#6f6", 5);
	}
}

function OnMouseMove(e){

	let mousepos = new Vec2(e.offsetX, e.offsetY);
	if(mainPoly == null){
		GenerateMainPolygon(mousepos);
	}
	else{
		let dif = mousepos.Subtract(mainPoly._transform._translation);
		mainPoly.Translate(dif);
	}

	ClearScreen();
	RenderPolygons();
	mainPoly.RenderFill(ctx, "#aaf")
	RenderPolygonCollisions();
}

Init();