/**@type {HTMLCanvasElement} */
var canvas = document.getElementById("exCanvas");
/**@type {CanvasRenderingContext2D} */
var ctx = canvas.getContext("2d");

/**@type {Array<Rect>} */
var rects = [];
/**@type {Array<Ray2>} */
var rays = [];
/**@type {Vec2} */
var raySpawnPoint = null;

function Init(){

	// generate some rects for the ray to test collision against
	GenerateRects(100);

	// set the OnMouseDown function to be called when the canvas is clicked on
	canvas.addEventListener("mousedown", OnMouseDown);
	canvas.addEventListener("touchstart", OnMouseDown);
	canvas.addEventListener("mousemove", OnMouseHover);
	canvas.addEventListener("touchmove", OnMouseHover);
}

/**
 * renders all the primitive objects on screen and the collision testing data
 */
function Redraw(){

	// clear the canvas to white
	ctx.fillStyle = "#fff";
	ctx.fillRect(0, 0, canvas.width, canvas.height);

	DrawRayCollisions();
}

function drawRectOutlines(){
	for(let i = rects.length - 1; i >= 0; i--){
		rects[i].RenderOutline(ctx, "#333", 1);
	}
}

function drawRay2s(){

	for(let i = rays.length - 1; i >= 0; i--){
		rays[i].RenderLine(ctx, "#f00", 1);
	}
}

/**
 * checks the ray vs box and ray vs ray intersections
 */
function DrawRayCollisions(){

	let rayintersects = [];
	for(let i = rays.length - 1; i >= 0; i--){

		// fill rects with blue that intersect with ray
		for(let j = rects.length - 1; j >= 0; j--){
			if(rays[i].IntersectsRect(rects[j])){
				rects[j].RenderFill(ctx, "#aaf");
			}
		}

		// draw ray vs ray intersection points
		for(let j = i - 1; j >= 0; j--){
			let intersect = rays[i].Intersection(rays[j]);
			if(intersect != null){
				rayintersects.push(intersect);
			}
		}
	}

	drawRectOutlines();
	drawRay2s();

	for(let i = rayintersects.length - 1; i >= 0; i--){
		rayintersects[i].RenderPoint(ctx, "#0a0", 3);
	}
}

/**
 * draws all the collisions that occur from a single ray
 * @param {Ray2} ray
 */
function DrawSingleRayCollision(ray){

	// fill rects with blue that intersect with ray
	for(let i = rects.length - 1; i >= 0; i--){
		if(ray.IntersectsRect(rects[i])){
			rects[i].RenderFill(ctx, "#ccf");
		}
	}
	
	ray.RenderLine(ctx, "#faa", 1);

	// draw ray vs ray intersection points
	for(let i = rays.length - 1; i >= 0; i--){
		let intersect = ray.Intersection(rays[i]);
		if(intersect != null){
			intersect.RenderPoint(ctx, "#0a0", 3);
		}
	}
}

/**
 * generates the specified number of random rect objects inside the canvas bounds
 * @param {Number} count 
 */
function GenerateRects(count){

	// how far from the edge of the canvas should rects spawn?
	padding = 10;
	// how large should the rects be?
	size = 5;

	rects = [];
	for(let i = 0; i < count; i++){

		// create a random rect and put it in the rect array
		let msize = Math.random() * size + 5;
		let point = new Vec2(Math.random() * (canvas.clientWidth - padding * 2) + padding, Math.random() * (canvas.clientHeight - padding * 2) + padding);
		let rect = new Rect(point.Subtract(new Vec2(msize)), point.Add(new Vec2(msize)));
		rects.push(rect);
	}
}

/**
 * spawns a ray from raySpawnPoint to the specified end point and adds it to the ray array
 * @param {Vec2} endpoint 
 */
function SpawnRay(endpoint){
	let ray = Ray2.FromPoints(raySpawnPoint, endpoint);
	raySpawnPoint = null;
	rays.push(ray);

	Redraw();
}

/**
 * set up to be called when the canvas is clicked
 * @param {MouseEvent} e 
 */
function OnMouseDown(e){

	let mousepos = new Vec2(e.offsetX, e.offsetY);
	if(raySpawnPoint == null){
		raySpawnPoint = mousepos;
		raySpawnPoint.RenderPoint(ctx, "#f00", 2);
	}else{
		SpawnRay(mousepos);
	}
}

function OnMouseHover(e){

	if(raySpawnPoint != null){
		let mousepos = new Vec2(e.offsetX, e.offsetY);
		let dif = mousepos.Subtract(raySpawnPoint);
		let ray = new Ray2(raySpawnPoint, dif, dif.magnitude)

		Redraw();
		DrawSingleRayCollision(ray);
	}
}

Init();
Redraw();