/**@type {HTMLCanvasElement} */
var canvas = document.getElementById("exCanvas");
/**@type {CanvasRenderingContext2D} */
var ctx = canvas.getContext("2d");

/**@type {Number} */
var _previousTimestamp = 0;
/**@type {Number}*/
var deltaTime = 0;

/** @type {Array<DynamicPolygon>} */
var polys = [];
/** @type {Array<Transform>} an array of transforms that the polygons will be transformed by each frame */
var deltaTransforms = [];

function Init(){

	GeneratePolygons();
	requestAnimationFrame(Step);
}
Init();

/**
 * Randomly generates all the polygons and their corresponding
 * deltaTransforms 
 * @param {Number} count
 */
function GeneratePolygons(count = 50){

	for(let i = count; i > 0; i--){

		let verts = Math.random() * 5 + 3;
		let size = Math.random() * 15 + 5;
		let poly = Polygon.CreateCircle(new Vec2(canvas.width / 2, canvas.height / 2), verts, size).ForceToDynamic(true);
		polys.push(poly);

		let speed = Math.random() * 0.1 + 0.05; // pixels per millisecond
		let rotSpeed = (Math.random() - 0.1) * 0.01; // radians per millisecond
		let trans = new Transform(Vec2.FromAngle(Math.random() * Math.PI * 2, speed), rotSpeed, new Vec2(1));
		deltaTransforms.push(trans);
	}
}

/**
 * Applies each delta transform to it's corresponding polygon 
 */
function TransformPolygons(){

	for(let i = polys.length - 1; i >= 0; i--){
		let poly = polys[i];
		let delta = deltaTransforms[i];

		// scale the transform by the delta time so it isn't 
		// dependent on a constant framerate
		let delta2 = delta.clone;
		delta2.translation = delta2.translation.Multiply(deltaTime);
		delta2.rotation = delta2.rotation * deltaTime;

		// apply the transformation
		poly.Transform(delta2);
	}
}

/**
 * makes the polygon bounce off the canvas edges
 */
function EnforcePolygonBounds(){
	
	let bounds = new Rect(new Vec2(), new Vec2(canvas.width, canvas.height));

	for(let i = polys.length - 1; i >= 0; i--){

		let polyrect = polys[i].boundingBox;
		let delta = deltaTransforms[i];
		let bounced = false;

		// ceiling collision
		if(polyrect.top <= bounds.top){

			// ensure delta translation y points downward
			delta._translation.y = Math.abs(delta._translation.y);
			bounced = true;
		}
		// floor collision
		else if(polyrect.bottom >= bounds.bottom){

			// ensure delta translation y points upward
			delta._translation.y = -Math.abs(delta._translation.y);
			bounced = true;
		}
		// left wall collision
		if(polyrect.left <= bounds.left){
			
			// ensure delta translation x points to the right
			delta._translation.x = Math.abs(delta._translation.x);
			bounced = true;
		}
		// right wall collision
		if(polyrect.right >= bounds.right){
			
			// ensure delta translation x points to the left
			delta._translation.x = -Math.abs(delta._translation.x);
			bounced = true;
		}

		// if it bounced off at least one wall this frame, reverse rotational direction
		if(bounced){
			delta.rotation *= -1;
		}
	}
}

/**
 * draw's the polygons onto the canvas
 */
function RenderPolygons(){
	
	for(let i = polys.length - 1; i >= 0; i--){
		polys[i].boundingBox.RenderOutline(ctx, "#AAA", 1)
		polys[i].RenderFill(ctx, "rgba(0,0,0,0.5)");
		polys[i].RenderOutline(ctx, "#000", 2);
	}
}

/**
 * Handles all function calls that are necessary for every time
 * the state of the program changes
 */
function Step(){
	
	Update();
	Render();
	requestAnimationFrame(Step);

	let now = performance.now();
	deltaTime = now - _previousTimestamp;
	_previousTimestamp = now;
}
function Update(){

	TransformPolygons();
	EnforcePolygonBounds();
}
function Render(){

	ctx.fillStyle = "#fff";
	ctx.fillRect(0, 0, canvas.width, canvas.height);
	RenderPolygons();
}