/**@type {HTMLCanvasElement} */
var canvas = document.getElementById("exCanvas");
/**@type {CanvasRenderingContext2D} */
var ctx = canvas.getContext("2d");

/**@type {Composite} */
var baseComposite = null;
/**@type {DynamicPolygon} */
var boolOpShape = null;

function Init(){

	baseComposite = new Composite([Polygon.CreateCircle(new Vec2(canvas.width / 2, canvas.height / 2), 12, 35)]);
	boolOpShape = Polygon.CreateCircle(new Vec2(canvas.width / 2, canvas.height / 2), 8, 20).ForceToDynamic(true);

	Render();

	canvas.addEventListener("mousemove", onMouseMove);
	canvas.addEventListener("mousedown", onMouseDown);
}
Init();

/**
 *  unifies the boolOpShape with the base composite 
 */
function Unify(){
	baseComposite = BooleanOperation.Union(baseComposite, boolOpShape);
}

/**
 * @param {MouseEvent} e 
 */
function onMouseMove(e){
	
	let mousepos = new Vec2(e.offsetX, e.offsetY);
	let dif = mousepos.Subtract(boolOpShape.vertexAverage);
	boolOpShape.Translate(dif);

	Render();
}
function onMouseDown(e){

	onMouseMove(e);
	Unify();

	Render();
}

function Render(){

	ctx.fillStyle = "#FFF";
	ctx.fillRect(0, 0, canvas.width, canvas.height)

	baseComposite.RenderFill(ctx, "#AAA");
	baseComposite.RenderOutline(ctx, "#000", 2);
	boolOpShape.RenderOutline(ctx, "#0A0", 1);
	boolOpShape._boundingBox.RenderOutline(ctx, "#00A", 1);
	
	baseComposite._polys[0].RenderNormals(ctx, "#066", 5);
	boolOpShape.RenderNormals(ctx, "#066", 5);

	let cols = PolygonCollision.CalculatePolygonCollisions(baseComposite._polys[0], boolOpShape)
	for(let i = cols.length - 1; i >= 0; i--){
		cols[i]._intersectPoint.RenderPoint(ctx, "#B00", 3)
	}
}