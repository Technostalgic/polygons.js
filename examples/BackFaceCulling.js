/**@type {HTMLCanvasElement} */
var canvas = document.getElementById("exCanvas");
/**@type {CanvasRenderingContext2D} */
var ctx = canvas.getContext("2d");

/**@type {Polygon} */
var poly = null;
/**@type {Ray2} */
var ray = null;

function Init(){

	poly = Polygon.CreateCircle(new Vec2(canvas.width / 2, canvas.height / 2), 16, 75);
	ray = Ray2.FromPoints(new Vec2(0), new Vec2(canvas.width / 2, canvas.height / 2));

	Render();

	canvas.addEventListener("mousemove", onMouseMove);
}
Init();

/**
 * @param {MouseEvent} e 
 */
function onMouseMove(e){
	
	let mousepos = new Vec2(e.offsetX, e.offsetY);
	ray = Ray2.FromPoints(mousepos, new Vec2(canvas.width / 2, canvas.height / 2));

	Render();
}

function Render(){

	ctx.fillStyle = "#FFF";
	ctx.fillRect(0, 0, canvas.width, canvas.height)
	
	poly.RenderOutline(ctx, "#D00", 2);
	poly.RenderNormals(ctx, "#0AA", 5);
	ray.RenderLine(ctx, "#999", 2);
	for(let i = poly._edges.length - 1; i >= 0; i--){
		if(Vec2.InwardFacing(poly.GetNormal(i), ray.directionNormal)){
			poly._edges[i].RenderLine(ctx, "#0A0", 4);
		}
	}
}